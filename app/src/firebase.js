import firebase from "firebase";
import "firebase/database";

const firebaseConfig = {
    apiKey: "AIzaSyCpM5aCIyiG5FJqdVSFPM1VpeeqTejY97s",
    authDomain: "dxc-ui.firebaseapp.com",
    databaseURL: "https://dxc-ui-default-rtdb.firebaseio.com",
    projectId: "dxc-ui",
    storageBucket: "dxc-ui.appspot.com",
    messagingSenderId: "1039332136982",
    appId: "1:1039332136982:web:7e110a71063511b61f7d37"
};

firebase.initializeApp(firebaseConfig);
export default firebase.database();