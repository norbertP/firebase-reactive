export var aiaMixin = {

    beforeCreate: function () {
        if (this.$options.aia) {
            this.addData = {};
            for (const k in this.$options.aia) {
                this.addData[k] = undefined;
            }
        }
    },

    created: function () {
        if (this.$options.aia) {
            for (const k in this.$options.aia) {
                const f = this.$options.aia[k];
                if (this.aia_watchers[k]) this.watchers[k]();

                const value = f.call(this);
                if (value) this['$' + value[0]](k, value[1], value[2]);

                this.aia_watchers[k] = this.$watch(function () {
                    return f.call(this);
                }, (value) => {
                    if (value) this['$' + value[0]](k, value[1], value[2]);
                });
            }
        }
    },
    beforeDestroy() {
        for (const w in this.aia_watchers) this.aia_watchers[w]();
        for (const h in this.aia_handlers) this.aia_handlers[h].close();
    },
    data() {
        const data = {
            //aia: aia,
            aia_watchers: {},
            aia_handlers: {},
        };
        data['aia_SUBS'] = {}

        Object.assign(data, this.addData);
        return data;
    },
    methods: {
        $one: function (k, method, args) {
            if (this.aia_handlers[k]) this.aia_handlers[k].close();
            this.aia_handlers[k] = this.aia.subscribe({mode: 'one', target: this, key: k}, method, args);
        },
        $all: function (k, method, args) {
            if (this.aia_handlers[k]) this.aia_handlers[k].close();
            this.aia_handlers[k] = this.aia.subscribe({mode: 'all', target: this, key: k}, method, args);
        },
    }
};