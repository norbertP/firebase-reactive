'use strict'

import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        layout: 'inbox-layout',
        searchOptions: {
            contract : []
        }
    },
    mutations: {
        SET_LAYOUT (state, payload) {
            state.layout = payload
        }
    },
    getters: {
        layout (state) {
            return state.layout
        },
        getSearchOptions : (state) => (type) => {
            return state.searchOptions[type]
        }

    }
})