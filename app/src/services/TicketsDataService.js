import firebase from "../firebase";

const db = firebase.ref("/tickets");

class TicketDataService {
    getOne(key) {
        return db.child(key);
    }

    getAll() {
        return db;
    }

    getAllStatus(status)
    {
        return db.orderByChild("status").equalTo(status)
    }

    create(ticket) {
        return db.push(ticket);
    }

    update(key, value) {
        return db.child(key).update(value);
    }

    delete(key) {
        return db.child(key).remove();
    }

    deleteAll() {
        return db.remove();
    }
}

export default new TicketDataService();