import { shallowMount } from '@vue/test-utils'
import SearchInput from "@/components/Business/Contract/Search/SearchInput";

describe('SearchInput', () => {
    it('renders a div', () => {
        const wrapper = shallowMount(SearchInput)
        const div = wrapper.find('div')
        expect(div.exists()).toBe(true)
    })

    it('renders a VTextField', () => {
        const wrapper = shallowMount(SearchInput)
        const div = wrapper.find('VTextField')
        expect(div.exists()).toBe(true)
    })
})
